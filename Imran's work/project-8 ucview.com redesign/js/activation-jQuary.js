/* scrollUp Minimum setup */
$(function () {
	$.scrollUp({
	scrollName: 'scrollUp', // Element ID
	topDistance: '500', // Distance from top before showing element (px)
	topSpeed: 700, // Speed back to top (ms)
	animation: 'slide', // Fade, slide, none
	animationInSpeed: 700, // Animation in speed (ms)
	animationOutSpeed: 700, // Animation out speed (ms)
	scrollText: 'Scroll to top', // Text for element
	activeOverlay: true, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
  });
});

	
	/* form counter tab */
$(document).ready(function(){	
	
	/* next button action */
	$("#next1").click(function(){
		$("#form-progressbar li:nth-child(2)").addClass("active");
		$(".background-form").addClass("display-none").removeClass("display-block");
		$(".OS-form").addClass("display-block").removeClass("display-none");
    });
	
	$("#next2").click(function(){
		$("#form-progressbar li:nth-child(3)").addClass("active");
		$(".OS-form").addClass("display-none").removeClass("display-block");
		$(".usage-form").addClass("display-block").removeClass("display-none");
    });
	
	$("#next3").click(function(){
		$("#form-progressbar li:nth-child(4)").addClass("active");
		$(".usage-form").addClass("display-none").removeClass("display-block");
		$(".webinar-form").addClass("display-block").removeClass("display-none");
    });
	
	$("#next4, #skip").click(function(){
		$("#form-progressbar li:nth-child(5)").addClass("active");
		$(".webinar-form").addClass("display-none").removeClass("display-block");
		$(".download-form").addClass("display-block").removeClass("display-none");
    });
	
	
	/* Previous button action */
	$("#previous1").click(function(){
		$("#form-progressbar li:nth-child(2)").removeClass("active");
		$(".background-form").addClass("display-block").removeClass("display-none");
		$(".OS-form").addClass("display-none").removeClass("display-block");
    });
	
	$("#previous2").click(function(){
		$("#form-progressbar li:nth-child(3)").removeClass("active");
		$(".OS-form").addClass("display-block").removeClass("display-none");
		$(".usage-form").addClass("display-none").removeClass("display-block");
    });
	
	$("#previous3").click(function(){
		$("#form-progressbar li:nth-child(4)").removeClass("active");
		$(".usage-form").addClass("display-block").removeClass("display-none");
		$(".webinar-form").addClass("display-none").removeClass("display-block");
    });
	
	$("#previous4").click(function(){
		$("#form-progressbar li:nth-child(5)").removeClass("active");
		$(".webinar-form").addClass("display-block").removeClass("display-none");
		$(".download-form").addClass("display-none").removeClass("display-block");
    });
	
	
});


/* datetime picker activation */
	$(function () {
		$('#datetimepicker1').datetimepicker({
			useCurrent:true,
			format:"DD MMM YYYY",
			ignoreReadonly:true,
			minDate: new Date()
			
			/* if i want to select client only specify date then the code will be
			minDate: "2016-02-20",
			maxDate: "2016-02-25" */
		});
	});
