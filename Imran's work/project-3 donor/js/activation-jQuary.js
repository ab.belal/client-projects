$(document).ready(function() {
     
      $(".donor-carousel").owlCarousel({    
          autoPlay: 5000, //Set AutoPlay to 3 seconds    
          items : 4,
          slideSpeed : 1200,
          paginationSpeed : 1200,
          pagination : false,
          stopOnHover : true,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]
     
      });
	  //end donor carousel
	 $(function(){
		$('.circlestat').circliful();
	});
	//end
	
     
    });