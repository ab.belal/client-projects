
    $(document).ready(function() {
     
      $(".books-carousel-wrapper").owlCarousel({    
          autoPlay: 3000, //Set AutoPlay to 3 seconds    
          items : 4,
          slideSpeed : 900,
          paginationSpeed : 900,
          pagination : false,
          stopOnHover : true,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]
     
      });
     
    });

